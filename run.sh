#!/bin/bash

source config.sh

for r in `seq 1 $RUNS`
do
echo "########################################"
echo "           RUN $r                       "
echo "########################################"
for CONG in "${CONGESTIION_CONTROLS[@]}"
do
  for IPERF_BUFFER_LENGTH in "${IPERF_BUFFER_LENGTHS[@]}"
  do
    for winc in "${WMEM_INCS[@]}"
    do
    for rinc in "${RMEM_INCS[@]}"
    do
      for ME in "${MPTCP_ENABLE[@]}"
      do

      	for SCH in "${SCHEDULER[@]}"
      	do

set +e
vagrant halt
#set -e
vagrant up
vagrant provision

SERVER_IP=`vagrant ssh server -c "ip addr show eth0 | grep -Po 'inet \K[\d.]+'" | tr -d '[:space:]'`
CLIENT_IP=`vagrant ssh client -c "ip addr show eth0 | grep -Po 'inet \K[\d.]+'" | tr -d '[:space:]'`
EXP_CLIENT_IP=`vagrant ssh client -c "ip addr show eth1 | grep -Po 'inet \K[\d.]+'" | tr -d '[:space:]'`
ENB0_IP=`vagrant ssh enb0 -c "ip addr show eth0 | grep -Po 'inet \K[\d.]+'" | tr -d '[:space:]'`
ENB1_IP=`vagrant ssh enb1 -c "ip addr show eth0 | grep -Po 'inet \K[\d.]+'" | tr -d '[:space:]'`

SL_SERVER_IP=$SERVER_IP
SL_CLIENT_IP=$CLIENT_IP
SL_EXP_CLIENT_IP=$EXP_CLIENT_IP
SL_ENB0_IP=$ENB0_IP
SL_ENB1_IP=$ENB1_IP

export SL_SERVER_IP
export SL_CLIENT_IP
export SL_EXP_CLIENT_IP
export SL_ENB0_IP
export SL_ENB1_IP

declare -a arr=("server" "enb0" "enb1")
for s in "${arr[@]}"
do
  echo "Clean up /var/log/kern.log on $s"
  vagrant ssh ${s} -c "echo > /dev/null | sudo tee /var/log/kern.log"
  vagrant ssh ${s} -c "sudo systemctl stop systemd-journald systemd-journald-dev-log.socket systemd-journald-audit.socket systemd-journald.socket"
done

# tcp write buffer min - default - max
w_init=128;  
w1=`expr $w_init \* $winc`; 
w2=`expr $w1 \* 4`; 
w3=`expr $w1 \* 1024`; 

# tcp read buffer min - default - max
r_init=128;  
r1=`expr $r_init \* $rinc`; 
r2=`expr $r1 \* 21`; 
r3=`expr $r1 \* 1536`; 

# net.core.rmem_default
c1=`expr 2 \* $r3 + 2 \* $w3`
# net.core.rmem_max
c2=`expr 2 \* $c1`

# net.ipv4.tcp_mem (in pages)
g1=`expr $c1 \/ 4096`
# set a minimum
if [ "$g1" -lt "45438" ]; then
        g1=45438
fi
g2=`expr 2 \* $g1`
g3=`expr 2 \* $g2`

# Set mptcp enabl/disable
ssh vagrant@${SERVER_IP} "sudo sysctl -w net.mptcp.mptcp_debug=1; sudo sysctl -w net.mptcp.mptcp_checksum=0; sudo sysctl -w net.mptcp.mptcp_enabled=${ME}"
RES=`ssh vagrant@${SERVER_IP} "sudo sysctl net.mptcp.mptcp_enabled | grep \"${ME}\""`

if [ -z "$RES" ]; then
  echo "MPCTP Enabled setting failed."
  exit -1
fi


ssh vagrant@${SERVER_IP} "sudo sysctl -w net.mptcp.mptcp_scheduler=${SCH}"
RES=`ssh vagrant@${SERVER_IP} "sudo sysctl net.mptcp.mptcp_scheduler | grep \"${SCH}\""`

if [ -z "$RES" ]; then
  echo "MPCTP scheduler setting failed."
  exit -1
fi

net_core_rmem_max=$c2
ssh vagrant@${SERVER_IP} "sudo sysctl -w net.core.rmem_max=$net_core_rmem_max"
RES=`ssh vagrant@${SERVER_IP} "sudo sysctl net.core.rmem_max | grep $net_core_rmem_max"`
if [ -z "$RES" ]; then
  echo "net.core.rmem_max setting failed."
  exit -1
fi

ssh vagrant@${CLIENT_IP} "sudo sysctl -w net.core.rmem_max=$net_core_rmem_max"
RES=`ssh vagrant@${CLIENT_IP} "sudo sysctl net.core.rmem_max | grep $net_core_rmem_max"`
if [ -z "$RES" ]; then
  echo "net.core.rmem_max setting failed."
  exit -1
fi

net_core_wmem_max=$c2
ssh vagrant@${SERVER_IP} "sudo sysctl -w net.core.wmem_max=$net_core_wmem_max"
RES=`ssh vagrant@${SERVER_IP} "sudo sysctl net.core.wmem_max | grep $net_core_wmem_max"`
if [ -z "$RES" ]; then
  echo "net.core.wmem_max setting failed."
  exit -1
fi

ssh vagrant@${CLIENT_IP} "sudo sysctl -w net.core.wmem_max=$net_core_wmem_max"
RES=`ssh vagrant@${CLIENT_IP} "sudo sysctl net.core.wmem_max | grep $net_core_wmem_max"`
if [ -z "$RES" ]; then
  echo "net.core.wmem_max setting failed."
  exit -1
fi

net_core_rmem_default=$c1
ssh vagrant@${SERVER_IP} "sudo sysctl -w net.core.rmem_default=$net_core_rmem_default"
RES=`ssh vagrant@${SERVER_IP} "sudo sysctl net.core.rmem_default | grep $net_core_rmem_default"`
if [ -z "$RES" ]; then
  echo "net.core.rmem_default setting failed."
  exit -1
fi

ssh vagrant@${CLIENT_IP} "sudo sysctl -w net.core.rmem_default=$net_core_rmem_default"
RES=`ssh vagrant@${CLIENT_IP} "sudo sysctl net.core.rmem_default | grep $net_core_rmem_default"`
if [ -z "$RES" ]; then
  echo "net.core.rmem_default setting failed."
  exit -1
fi

net_core_wmem_default=$c1
ssh vagrant@${SERVER_IP} "sudo sysctl -w net.core.wmem_default=$net_core_wmem_default"
RES=`ssh vagrant@${SERVER_IP} "sudo sysctl net.core.wmem_default | grep $net_core_wmem_default"`
if [ -z "$RES" ]; then
  echo "net.core.wmem_default setting failed."
  exit -1
fi

ssh vagrant@${CLIENT_IP} "sudo sysctl -w net.core.wmem_default=$net_core_wmem_default"
RES=`ssh vagrant@${CLIENT_IP} "sudo sysctl net.core.wmem_default | grep $net_core_wmem_default"`
if [ -z "$RES" ]; then
  echo "net.core.wmem_default setting failed."
  exit -1
fi

ssh vagrant@${SERVER_IP} "sudo sysctl -w net.ipv4.tcp_mem='$g1 $g2 $g3'"
RES=`ssh vagrant@${SERVER_IP} "sudo sysctl net.ipv4.tcp_mem | grep \"$g1[[:space:]]$g2[[:space:]]$g3\""`
if [ -z "$RES" ]; then
  echo "net.ipv4.tcp_mem setting failed."
  exit -1
fi

ssh vagrant@${CLIENT_IP} "sudo sysctl -w net.ipv4.tcp_mem='$g1 $g2 $g3'"
RES=`ssh vagrant@${CLIENT_IP} "sudo sysctl net.ipv4.tcp_mem | grep \"$g1[[:space:]]$g2[[:space:]]$g3\""`
if [ -z "$RES" ]; then
  echo "net.ipv4.tcp_mem setting failed."
  exit -1
fi

# Set socket buffer
ssh vagrant@${SERVER_IP} "sudo sysctl -w net.ipv4.tcp_wmem='$w1 $w2 $w3'"
RES=`ssh vagrant@${SERVER_IP} "sudo sysctl net.ipv4.tcp_wmem | grep \"$w1[[:space:]]$w2[[:space:]]$w3\""`
if [ -z "$RES" ]; then
  echo "WMEM Socket buffer setting failed."
  exit -1
fi

ssh vagrant@${SERVER_IP} "sudo sysctl -w net.ipv4.tcp_rmem='$r1 $r2 $r3'"
RES=`ssh vagrant@${SERVER_IP} "sudo sysctl net.ipv4.tcp_rmem | grep \"$r1[[:space:]]$r2[[:space:]]$r3\""`
if [ -z "$RES" ]; then
  echo "WMEM Socket buffer setting failed."
  exit -1
fi


ssh vagrant@${CLIENT_IP} "sudo sysctl -w net.mptcp.mptcp_debug=1; sudo sysctl -w net.mptcp.mptcp_checksum=0; sudo sysctl -w net.ipv4.tcp_rmem='$r1 $r2 $r3'"
RES=`ssh vagrant@${CLIENT_IP} "sudo sysctl net.ipv4.tcp_rmem | grep \"$r1[[:space:]]$r2[[:space:]]$r3\""`

if [ -z "$RES" ]; then
  echo "RMEM Socket buffer setting failed."
  exit -1
fi

ssh vagrant@${CLIENT_IP} "sudo sysctl -w net.ipv4.tcp_wmem='$w1 $w2 $w3'"
RES=`ssh vagrant@${CLIENT_IP} "sudo sysctl net.ipv4.tcp_wmem | grep \"$w1[[:space:]]$w2[[:space:]]$w3\""`
if [ -z "$RES" ]; then
  echo "WMEM Socket buffer setting failed."
  exit -1
fi


# Set congestion control
ssh vagrant@${SERVER_IP} "sudo sysctl -w net.ipv4.tcp_congestion_control=${CONG}"

RES=`ssh vagrant@${SERVER_IP} "sudo sysctl net.ipv4.tcp_congestion_control | grep \"${CONG}\""`
if [ -z "$RES" ]; then
  echo "Congestion control setting failed."
  exit -1
fi

ssh vagrant@${CLIENT_IP} "sudo sysctl -w net.ipv4.tcp_congestion_control=${CONG}"

RES=`ssh vagrant@${CLIENT_IP} "sudo sysctl net.ipv4.tcp_congestion_control | grep \"${CONG}\""`
if [ -z "$RES" ]; then
  echo "Congestion control setting failed."
  exit -1
fi


ssh vagrant@${SERVER_IP} "sudo tc qdisc add dev eth1 root fq"
RES=`ssh vagrant@${SERVER_IP} "sudo tc qdisc show dev eth1 | grep \"qdisc fq\""`
if [ -z "$RES" ]; then
  echo "qdisc setting failed."
  exit -1
fi

export SL_IPERF_BUFFER_LENGTH=${IPERF_BUFFER_LENGTH}

timeout 360 /home/rluebben/.local/bin/sshlauncher -d run.sl

rm -rf tmp_results
mkdir tmp_results
cd tmp_results

#collect results
declare -a arr=("enb0" "enb1")
for s in "${arr[@]}"
do
  echo $s
  vagrant ssh ${s} -c "sudo cp /var/log/kern.log /home/vagrant; sudo chown vagrant /home/vagrant/kern.log"
  vagrant scp ${s}:/home/vagrant/kern.log ./${s}_kern.log
  vagrant scp ${s}:~/${s}_ingress.pcap ./
  vagrant scp ${s}:~/${s}_egress.pcap ./a
  vagrant ssh ${s} -c "sudo sysctl -a" > sysctl_${s}.log
  vagrant ssh ${s} -c "sudo rm /var/log/kern.log"
  vagrant ssh ${s} -c "sudo rm /home/vagrant/kern.log; sudo rm *.pcap"
done

#collect results
declare -a arr=("server")
for s in "${arr[@]}"
do
  echo $s
  vagrant ssh ${s} -c "sudo cp /var/log/kern.log /home/vagrant; sudo chown vagrant /home/vagrant/kern.log"
  vagrant scp ${s}:/home/vagrant/kern.log ./${s}_kern.log
  vagrant scp ${s}:/home/vagrant/${s}.pcap ./
  vagrant scp ${s}:/home/vagrant/iperf3_sender.log ./
  vagrant scp ${s}:/home/vagrant/server_trace.txt ./
  vagrant ssh ${s} -c "sudo sysctl -a" > sysctl_${s}.log
  #vagrant scp ${s}:/home/vagrant/mytrace.txt ./
  #vagrant ssh ${s} -c "sudo rm mytrace.txt"
  vagrant ssh ${s} -c "sudo rm /home/vagrant/kern.log; sudo rm *.pcap; sudo rm iperf3_sender.log; sudo rm /var/log/kern.log; sudo /home/vagrant/server_trace.txt"
done

#collect results
declare -a arr=("client")
for s in "${arr[@]}"
do
  echo $s
  vagrant scp ${s}:/home/vagrant/iperf3_receiver.log ./
  vagrant scp ${s}:/home/vagrant/${s}.pcap ./
  vagrant ssh ${s} -c "sudo sysctl -a" > sysctl_${s}.log
  vagrant ssh ${s} -c "sudo rm iperf3_receiver.log"
done


cd ..
DIRN="$1_results_cong_${CONG}_wmem_${w3}_rmem_${r3}_isize_${IPERF_BUFFER_LENGTH}_mptcp_${ME}_scheduler_${SCH}_run_${r}"
rm -rf ${DIRN}
mv tmp_results ${DIRN}
echo $DIRN
      done
    done
  done
done
done
done
done

