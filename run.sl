[DEFAULT]
user: vagrant
password: vagrant

[client_tcpdump]
host: %(SL_CLIENT_IP)s
command: sudo tcpdump -i any  -s 150 -w client.pcap

[server_tcpdump]
host: %(SL_SERVER_IP)s
command: sudo tcpdump -i eth1 -s 150 -w server.pcap
#[server_trace]
#host: %(SL_SERVER_IP)s
#command: sudo timeout -s INT 50 trace-cmd record -p function -l *tcp*

[server_trace]
host: %(SL_SERVER_IP)s
command: sudo sh -c "echo 1 > /sys/kernel/debug/tracing/events/tcp/tcp_probe/enable"; sudo sh -c "echo \"sport == 5201 || dport == 5201\" > /sys/kernel/debug/tracing/events/tcp/tcp_probe/filter"; sudo sh -c "echo 1 > /sys/kernel/debug/tracing/tracing_on"; echo "tcp probe enabled"
#command: sudo sh -c "echo \"*tcp*\" > /sys/kernel/debug/tracing/set_ftrace_filter"; sudo sh -c "echo function > /sys/kernel/debug/tracing/current_tracer"; sudo sh -c "echo \"*tcp*\" > /sys/kernel/debug/tracing/set_ftrace_filter"; sudo sh -c "echo 1 > /sys/kernel/debug/tracing/events/tcp/tcp_probe/enable"; sudo sh -c "echo \"sport == 5201 || dport == 5201\" > /sys/kernel/debug/tracing/events/tcp/tcp_probe/filter"; sudo sh -c "echo 1 > /sys/kernel/debug/tracing/tracing_on"; echo "tcp probe enabled"

[server_trace_pipe]
host: %(SL_SERVER_IP)s
command: sudo sh -c "cat /sys/kernel/debug/tracing/trace_pipe > server_trace.txt"
after: { 'server_trace': 'tcp probe enabled'}


[enb1_tcpdump_ingress]
host: %(SL_ENB1_IP)s
command: sudo tcpdump -i eth1 -s 150 -w enb1_ingress.pcap

[enb1_tcpdump_egress]
host: %(SL_ENB1_IP)s
command: sudo tcpdump -i eth2 -s 150 -w enb1_egress.pcap

[enb0_tcpdump_ingress]
host: %(SL_ENB0_IP)s
command: sudo tcpdump -i eth1 -s 150 -w enb0_ingress.pcap

[enb0_tcpdump_egress]
host: %(SL_ENB0_IP)s
command: sudo tcpdump -i eth2 -s 150 -w enb0_egress.pcap

[client_iperf]
host: %(SL_CLIENT_IP)s
command: echo "Server listening"; timeout -s INT 60 iperf3 -s -i 0.1 -1 --logfile iperf3_receiver.log -J
after: { 'enb0_tcpdump_egress': 'listening on', 'enb0_tcpdump_ingress': 'listening on', 'enb1_tcpdump_egress': 'listening on', 'enb1_tcpdump_ingress': 'listening on', 'server_trace': 'tcp probe enabled' }

[server_trace_mark]
host: %(SL_SERVER_IP)s
command: sudo sh -c "echo `date +%s.%N` > /sys/kernel/debug/tracing/trace_marker"
after: { 'client_iperf' : 'Server listening' }

[server_iperf]
host: %(SL_SERVER_IP)s
command: iperf3 -c %(SL_EXP_CLIENT_IP)s -l %(SL_IPERF_BUFFER_LENGTH)s -i 0.1 -t 45 -J --logfile iperf3_sender.log; echo "Iperf finished"
after: { 'client_iperf' : 'Server listening' }

[enb0_kill_tcpdump]
host: %(SL_ENB0_IP)s
command: sudo killall tcpdump
after: {'server_iperf':'Iperf finished' }

[enb1_kill_tcpdump]
host: %(SL_ENB1_IP)s
command: sudo killall tcpdump
after: {'server_iperf':'Iperf finished' }

[server_kill_tcpdump]
host: %(SL_SERVER_IP)s
command: sudo killall tcpdump
after: {'server_iperf':'Iperf finished' }

[client_kill_tcpdump]
host: %(SL_CLIENT_IP)s
command: sudo killall tcpdump
after: {'server_iperf':'Iperf finished' }

[server_kill_trace_pipe]
host: %(SL_SERVER_IP)s
command: sudo pkill -f trace_pipe
after: {'server_iperf':'Iperf finished' }


