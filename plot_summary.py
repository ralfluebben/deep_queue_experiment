import numpy as np
import pandas as pd
import sys
import os
import json
import math
import matplotlib as mpl
if os.environ.get('DISPLAY','') == '':
    print('no display found. Using non-interactive Agg backend')
    mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import rcParams
import subprocess 

rcParams['font.family'] = 'serif'
rcParams['font.sans-serif'] = ['Times']

resultfile = sys.argv[1] + '.csv'
figname_pdf = "tcp_wmem_rmem_" + sys.argv[1] +".pdf"
figname_png = "tcp_wmem_rmem_" + sys.argv[1] +".png"

df=pd.read_csv(resultfile,sep=' ',names=['wmem', 'rmem','rate'], header=None)

lwmem=df.groupby('wmem').rate.apply(pd.Series.tolist)
lrmem=df.groupby('rmem').rate.apply(pd.Series.tolist)

num_groups=len(lwmem.keys())
num_bars=len(lrmem.keys())

ind = np.arange(num_groups)  # the x locations for the groups
width = 0.18  # the width of the bars

offset = int(num_bars/2)

fig, ax = plt.subplots()
for i in np.arange(num_bars):
	r=ax.bar(ind - offset*width,lrmem[lrmem.keys()[i]], width,  label=round(lrmem.keys()[i]/1048576.0,2))
	offset-=1

ax.set_xticks(ind)
y_max=math.ceil(df['rate'].max()/10)*10
ax.set_yticks(np.arange(0, y_max, step=10))
ax.set_ylim(0, y_max)

xlim_min=-width*num_bars/2-0.1
xlim_max=width*num_bars/2+num_groups-1+0.1
ax.set_xlim(xlim_min, xlim_max)

xtick=list(map(lambda x: round(x/1048576,2), lwmem.keys().values))
ax.set_xticklabels(xtick)
ax.set_ylabel('throughput [MBit/s]')
ax.set_xlabel('send buffer size [MByte]')
ax.legend(loc="upper left", title="recv buffer size")

x=(xlim_min, xlim_max)
y=(28.3, 28.3)
plt.plot(x, y, '--', color='k', )
y=(56.6, 56.6)
plt.plot(x, y, '--', color='k', )
fig.tight_layout()
fig.savefig(figname_png, bbox_inches='tight')
fig.savefig(figname_pdf, bbox_inches='tight')
