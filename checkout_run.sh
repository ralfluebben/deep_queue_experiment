# Set path to your MPTCP kernel
MPTCP_KERNEL_DIR=${1}
EXP_DIR=`pwd`

cd $MPTCP_KERNEL_DIR
tag=${2}
uid=`git log --pretty=format:'%h' -n 1`
knum=`cat .version`
kerneltag=`echo "-$tag-$uid-$knum" | awk '{print tolower($0)}' | sed 's/_/-/g' | sed 's#/#-#g'`

# get the kernel and compile
git pull
git checkout $tag
make -j`nproc` bindeb-pkg LOCALVERSION=$kerneltag

# install kernel
cd $EXP_DIR
uid2=`git log --pretty=format:'%h' -n 1`
vagrant halt
vagrant up
sleep 30
bash install_kernel.sh "$kerneltag"

# activate kernel 
gtag=`echo "$tag-$uid-$knum" | awk '{print tolower($0)}' | sed 's/_/-/g' | sed 's#/#-#g'`
bname=`vagrant ssh server -c  "grep menuentry /boot/grub/grub.cfg" | awk -F"'" '{print $4}' | grep "advanced" | grep $gtag`
# set default kernel image
# check if kernel was installed and loaded
declare -a arr=("client" "server")
for s in "${arr[@]}"
do
  vagrant ssh ${s} -c "sudo grub-set-default $bname; sudo update-grub; sudo rm *.deb;sudo poweroff"
  vagrant halt ${s}
  vagrant up ${s}
  sleep 30
  kernelloaded=`vagrant ssh ${s} -c "uname -a"`
  if [[ ${kernelloaded} != *$gtag* ]];then
    echo "Kernel not loaded on $s."
    exit -1
  fi
done

# run experiment
bash run.sh $gtag-${uid2}
bash read_rates.sh $gtag-${uid2}
for f in `cat result_file_list.txt`
do
  exp_name=`basename $f .csv`
  python plot_summary.py $exp_name
done
