#!/bin/bash

VER=$1
declare -a arr=("server" "client")
for s in "${arr[@]}"
do
  echo $s
  for f in `find ~/git -maxdepth 1 -name "*${VER}*deb"`
  do
  	vagrant scp $f ${s}:/home/vagrant/
  done
  vagrant ssh ${s} -c "sudo dpkg -i *${VER}*deb"
done

