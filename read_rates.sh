#!/bin/bash

source config.sh

rm result_file_list.tmp
rm result_file_list.txt
rm *results*.csv

for CONG in "${CONGESTIION_CONTROLS[@]}"
do
  for IPERF_BUFFER_LENGTH in "${IPERF_BUFFER_LENGTHS[@]}"
  do
    for winc in "${WMEM_INCS[@]}"
    do
    for rinc in "${RMEM_INCS[@]}"
    do
      for ME in "${MPTCP_ENABLE[@]}"
      do
        for SCH in "${SCHEDULER[@]}"
        do

w_init=128;
w1=`expr $w_init \* $winc`;
w2=`expr $w1 \* 4`;
w3=`expr $w1 \* 1024`;

r_init=128;
r1=`expr $r_init \* $rinc`;
r2=`expr $r1 \* 21`;
r3=`expr $r1 \* 1536`;

exp_name="${1}_results_cong_${CONG}_wmem_${w3}_rmem_${r3}_isize_${IPERF_BUFFER_LENGTH}_mptcp_${ME}_scheduler_${SCH}_run_*"
result_filename="rates_${1}_results_cong_${CONG}_isize_${IPERF_BUFFER_LENGTH}_mptcp_${ME}_scheduler_${SCH}.csv"
   for d in `find ./ -name ${exp_name}`; do
     cd $d
     echo `pwd`
     res=`python ../read_rate_from_iperf3_json.py`
     echo "$w3 $r3 $res" >> ../$result_filename
     bash ../prepare_for_plotting.sh
     python ../plot.py
     echo "${result_filename}" >> ../result_file_list.tmp
     cd ..
  done
done
done
done
done
done
done
sort result_file_list.tmp | uniq > result_file_list.txt
