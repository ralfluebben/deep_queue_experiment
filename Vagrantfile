# -*- mode: ruby -*-

# vi: set ft=ruby :
$script_server = <<-SCRIPT
sudo ip route add 192.168.0.0/16 via 192.168.1.11
sudo ip link set dev eth0 multipath off
for i in `ip -o link show | awk -F': ' '{print $2}'`; do sudo ethtool -K $i tso off gro off gso off; done
SCRIPT

$script_gateway = <<-SCRIPT
sudo ip route add 192.168.4.0/24 via 192.168.2.12
sudo ip route add 192.168.5.0/24 via 192.168.3.12
sudo sh -c "echo 1 > /proc/sys/net/ipv4/ip_forward"
for i in `ip -o link show | awk -F': ' '{print $2}'`; do sudo ethtool -K $i tso off gro off gso off; done
SCRIPT

$script_enb0 = <<-SCRIPT
sudo ip route add 192.168.1.0/24 via 192.168.2.11
sudo sh -c "echo 1 > /proc/sys/net/ipv4/ip_forward"
sudo tc qdisc add dev eth1 root netem limit 3000 rate 30mbit delay 50ms
sudo tc qdisc add dev eth2 root netem limit 3000 rate 30mbit delay 50ms
for i in `ip -o link show | awk -F': ' '{print $2}'`; do sudo ethtool -K $i tso off gro off gso off; done
SCRIPT

$script_enb1 = <<-SCRIPT
sudo ip route add 192.168.1.0/24 via 192.168.3.11
sudo sh -c "echo 1 > /proc/sys/net/ipv4/ip_forward"
sudo tc qdisc add dev eth1 root netem limit 3000 rate 30mbit delay 50ms
sudo tc qdisc add dev eth2 root netem limit 3000 rate 30mbit delay 50ms
for i in `ip -o link show | awk -F': ' '{print $2}'`; do sudo ethtool -K $i tso off gro off gso off; done
SCRIPT

$script_client = <<-SCRIPT
sudo ip rule add from 192.168.4.13 table 1
sudo ip rule add from 192.168.5.13 table 2

# Configure the two different routing tables
sudo ip route add 192.168.4.0/24 dev eth1 table 1
sudo ip route add 192.168.0.0/16 via 192.168.4.12 table 1

sudo ip route add 192.168.5.0/24 dev eth2 table 2
sudo ip route add 192.168.0.0/16 via 192.168.5.12 table 2

# default route for the selection process of normal internet-traffic
sudo ip route add 192.168.0.0/16 via 192.168.4.12
sudo ip link set dev eth0 multipath off
for i in `ip -o link show | awk -F': ' '{print $2}'`; do sudo ethtool -K $i tso off gro off gso off; done
SCRIPT



boxes = [
    {
        :name => "server",
        :networks =>  [
          ['private_network', 'eth1',"192.168.1.10" ],
        ],
        :mem => "4096",
        :cpu => "2",
        :script => $script_server
    },
    {
        :name => "gateway",
        :networks =>  [
          ['private_network', 'eth1',"192.168.1.11" ],
          ['private_network', 'eth2',"192.168.2.11" ],
          ['private_network', 'eth3',"192.168.3.11" ],
        ],
        :mem => "2048",
        :cpu => "2",
        :script => $script_gateway
    },
    {
        :name => "enb0",
        :networks =>  [
          ['private_network', 'eth1',"192.168.2.12" ],
          ['private_network', 'eth2',"192.168.4.12" ],
        ],
        :mem => "2048",
        :cpu => "2",
        :script => $script_enb0
    },
    {
        :name => "enb1",
        :networks =>  [
          ['private_network', 'eth1',"192.168.3.12" ],
          ['private_network', 'eth2',"192.168.5.12" ],
        ],
        :mem => "2048",
        :cpu => "2",
        :script => $script_enb1
    },
    {
        :name => "client",
        :networks =>  [
          ['private_network', 'eth1',"192.168.4.13" ],
          ['private_network', 'eth2',"192.168.5.13" ],
        ],
        :mem => "4096",
        :cpu => "2",
        :script => $script_client
    }
    
]

Vagrant.configure(2) do |config|

  config.vm.box = "generic/debian10"

  # Turn off shared folders
  config.vm.synced_folder ".", "/vagrant", id: "vagrant-root", disabled: true

  boxes.each do |opts|
    config.vm.define opts[:name] do |config|
      config.vm.hostname = opts[:name]
      config.vm.provision "shell", inline: opts[:script]
      networks = opts[:networks]
      networks.each do |net_opts|
        net_opts
        config.vm.network net_opts[0], ip: net_opts[2]
      end
    end
  end
end

