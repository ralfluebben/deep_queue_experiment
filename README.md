These scripts are used to evaluate MPTCP in various scenearios especially for 
building the experiments presented in the paper 

**An Odd Couple: Loss-Based Congestion Control and Minimum RTT Scheduling in MPTCP;
R Lübben, J Morgenroth;
IEEE Conference on Local Computer Networks (LCN)**

updated results are at [Results](https://gitlab.com/ralfluebben/deep_queue_experiment/wikis/Results)


# Prerequisites (Tested with Ubuntu 18.04 LTS)
- Debian/Ubuntu packages installed to build a .deb package from the kernel source, see https://wiki.debian.org/BuildADebianKernelPackage
- Vagrant with libvirt plugin (https://github.com/vagrant-libvirt/vagrant-libvirt#installation)
- sshlauncher (https://github.com/bozakov/sshlauncher)
- tshark
- python with modules numpy, matplotlib, pandas
- vagrant plugin vagrant-scp
- Check for Vagrant default network used as control network, typically on virbr0 with ip range 192.168.122.0
- Create a local ssh config (~/.ssh/config) for the control network 192.168.121.0 to avoid ssh warning/erros:
```
Host 192.168.121.*
  UserKnownHostsFile /dev/null
  StrictHostKeyChecking no
```

# Prepare images
1. Setup images

Provising may fail during first setup with message `client: Error: either "dev" is duplicate, or "multipath" is a garbage.` , since the kernel does not support MPTCP configurtion yet. After a kernel with MPTCP is installed no error/warning should occur.
```
# Install basic images
vagrant up
# Install required software, MPTCP kernel and tools, ssh keys from host
bash prepare_vagrant_images.sh
```

2. Run the experiment
```
bash checkout_run.sh <mpctp_kernel_dir> <tag/branch name>
```
Replace `<mpctp_kernel_dir>` with the path to the MPTCP kernel and `<tag/branch name>` with the MPTCP kernel branch or tag you like to test. The script checks out the kernel, compiles it, builds debian packages, installs them to image, runs the experiment, and plots a graph for comparison and for each individual experiment.

Notes:
- To update the experiment settings adapt `config.sh`.
- If the kernel config is not complete, you will be ask to add new modules during kernel build.
- The scripts `checkout_run.sh` plots graphs for appl. throughput, network throughput, TCP internal states for each experiment. The plots are stored in the related experiment subfolder.
- The scripts `checkout_run.sh` plots a rate comparision, one graph for each tested congestion control protocol. Each graph compares for different sizes of write and read buffer sizes.

# Known Issues
  1. Instead of preparing the images manually a Vagrant box including all software would be beneficial, still building the box using libvirt on Ubuntu 18.04 failed for me.
