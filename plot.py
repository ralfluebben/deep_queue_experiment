import numpy as np
import pandas
import sys
import os
import json
import matplotlib as mpl
if os.environ.get('DISPLAY','') == '':
    print('no display found. Using non-interactive Agg backend')
    mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import rcParams
import subprocess 

mintime=float("inf") 
maxtime=-float("inf") 

providers = [ "_1", "_2", "" ]
providers_label = [ "sf 1", "sf 2", "sum" ]
ms_s=1000/100

rcParams['font.family'] = 'serif'
rcParams['font.sans-serif'] = ['Times']
rcParams['lines.markersize'] = 4
img_width = 10
img_height = 2*img_width
fig, axarr = plt.subplots(4, sharex=True)



print "########## Get tracing mark ################"
# Tracing mark is used to link kernel time (since boot) to real time, keep in mind that time may drift between used clocks
cmd = "grep tracing_mark_write server_trace.txt | sed 's/[[:space:]]\{1,\}/,/g'  | cut -d ',' -f 5,7 | sed 's/://g'"
po = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
ts=po.stdout.read()
s1,s2=ts.rstrip('\n').split(',')
ttprobe_ref=float(s1)
ttprobe_start=float(s2)

# Is used as reference time since userspace and kernel space clock may differ
diff_real_time_to_kernel_time=ttprobe_start

ts_start=None
print "########## Get network throughput ################"
p=0

c=0
for k in providers:
  #print k
  filename="throughput%s.csv" % (k)
  try:
    th=pandas.read_csv(filename, delimiter=" ").as_matrix()
    if ts_start == None:
      ts_start = th[0,0]
    mintime=np.floor(th[0,0]*ms_s)/ms_s
    maxtime=np.ceil(th[-1,0]*ms_s)/ms_s
    intervals=int(ms_s*(maxtime-mintime))+1
    ti=np.linspace(mintime,maxtime,intervals)
    
    th2=np.zeros([len(ti),2])
    th2[:,0]=ti
    i1=0
    i2=0
    i=0
    while i < intervals-2:
      while th[i2,0] < ti[i+1]:
        i2=i2+1
      i2=i2-1
      th2[i,1]=np.sum(th[i1:i2,1])
      i1=i2+1
      i=i+1
    th2[:,1]=th2[:,1]*ms_s*8/1e6
    ln = providers_label[c] #'throughput %s [MBit/s]' % (k)
    axarr[p].plot(th2[:,0]-diff_real_time_to_kernel_time,th2[:,1],'.', label=ln)
  except Exception as e:
    print(e)
  c += 1
axarr[p].set_ylabel('network throughput\n[MBit/s]')  
axarr[p].legend()
p+=1

'''
1 src
2 [::ffff:172.31.39.131]:5201
3 dest
4 [::ffff:80.187.108.54]:11400
5 mark
6 0x0
7 data_len
8 0
9 snd_nxt
10 2509549665
11 snd_una
12 2508674301
13 write_seq
14 2509602501
15 snd_cwnd
16 750
17 snd_cwnd_clamp
18 4294967295
19 packets_in_flight
20 613
21 ssthresh
22 2147483647
23 snd_wnd
24 2545664
25 srtt
26 58829
27 minrtt
28 36580
29 rcv_wnd
30 27136
31 sk_wmem_queued
32 2870400
33 sk_sndbuf
34 4194304
35 wnd_end
36 2511219
37 is_cwnd_limited 
38 0 
39 is_writeable 
40 1 
41 sock_cookie 
42 44 
43 mptcp 
44 1 
45 bbr_bw 
46 36174 
47 bbr_minrtt 
48 36307 
49 bbr_pacing_gain 
50 739 
51 bbr_cwnd_gain 
52 739 
53 bbr_mode
54 0
'''


'''
0 time
1 src
2 dest
3 mark
4 data_len
5 snd_nxt
6 snd_una
7 write_seq
8 snd_cwnd
9 snd_cwnd_clamp
10 packets_in_flight
11 ssthresh
12 snd_wnd
13 srtt
14 minrtt
15 rcv_wnd
16 sk_wmem_queued
17 sk_sndbuf
18 wnd_end
19 is_cwnd_limited 
20 is_writeable 
21 sock_cookie 
22 mptcp 
23 bbr_bw 
24 bbr_minrtt 
25 bbr_pacing_gain 
26 bbr_cwnd_gain 
27 bbr_mode
28 mptcp_snd_nxt
29 mptcp_snd_una
30 mptcp_write_seq
31 mptcp_is_writeable
32 mptcp_is_cwnd_limited
33 mptcp_sk_mem_queued
34 mptcp_rcv_wnd
35 bbr_pacing_rate
36 rtt_var
37 rto
'''

print "############# Read ttprobe values ###############"
cnames=['time', 'cwnd\n[pkts]', 'in_flight\n[kbyte]','srtt\n[ms]', 'rcv_wnd [byte]', 'sk_mem_queued\n[kbyte]', 'cwnd_limited', 'is_writeable', 'mp_snd_nxt', 'mp_snd_una', 'mp_write_seq', 'mp_is_writeable', 'mp_is_cwnd_limited', 'mp_sk_mem_queued', 'mp_rcv_wnd','rrtvar','rto']

filename="ttprobe_1.csv"
th_1=pandas.read_csv(filename, delimiter=" ", usecols=[0,8,10,13,15,16,19,20, 28, 29,30, 31,32,33,34,36,37 ], index_col=False, names = cnames).as_matrix()
th_1[:,0]=th_1[:,0]-ttprobe_ref

filename="server/ttprobe_2.csv"
filename="ttprobe_2.csv"
th_2=pandas.read_csv(filename, delimiter=" ", usecols=[0,8,10,13,15,16,19,20, 28, 29,30, 31,32,33,34,36,37], index_col=False, names = cnames).as_matrix()
th_2[:,0]=th_2[:,0]-ttprobe_ref

axarr[p].plot(th_1[:,0],th_1[:,3]/1000,'.')
axarr[p].plot(th_2[:,0],th_2[:,3]/1000,'.')
axarr[p].set_ylabel(cnames[3])
p+=1

axarr[p].plot(th_1[:,0],th_1[:,1],'.')
axarr[p].plot(th_2[:,0],th_2[:,1],'.')
axarr[p].set_ylabel(cnames[1])
p+=1
axarr[p].plot(th_1[:,0],th_1[:,2],'.')
axarr[p].plot(th_2[:,0],th_2[:,2],'.')
axarr[p].set_ylabel(cnames[2])

axarr[p].set_xlabel('time [s]')

fig.set_size_inches(img_width, img_height)
fig.tight_layout()
#plt.show()

fname = 'server_ttprobe_throughput.png'
fig.savefig(fname)
fname = 'server_ttprobe_throughput.pdf'
fig.savefig(fname)

#######################
## Client 
#######################

img_width = 10
img_height = 0.5*img_width
fig, axarr = plt.subplots(1, sharex=True)

ts_start=None
print "########## Get network throughput ################"
p=0
c=0
for k in providers:
  filename="client_throughput%s.csv" % (k)
  try:
    th=pandas.read_csv(filename, delimiter=" ").as_matrix()
    if ts_start == None:
      ts_start = th[0,0]
    mintime=np.floor(th[0,0]*ms_s)/ms_s
    maxtime=np.ceil(th[-1,0]*ms_s)/ms_s
    intervals=int(ms_s*(maxtime-mintime))+1
    ti=np.linspace(mintime,maxtime,intervals)
    
    th2=np.zeros([len(ti),2])
    th2[:,0]=ti
    i1=0
    i2=0
    i=0
    while i < intervals-2:
      while th[i2,0] < ti[i+1]:
        i2=i2+1
      i2=i2-1
      th2[i,1]=np.sum(th[i1:i2,1])
      i1=i2+1
      i=i+1
    th2[:,1]=th2[:,1]*ms_s*8/1e6
    ln = providers_label[c] #'throughput %s [MBit/s]' % (k)
    axarr.plot(th2[:,0]-ts_start,th2[:,1],'.', label=ln)
  except Exception as e:
    print(e)
  c += 1
axarr.set_ylabel('network throughput\n[MBit/s]')  
axarr.legend()
axarr.set_xlabel('time [s]')
axarr.set_xlim(0,45)

fig.set_size_inches(img_width, img_height)
fig.tight_layout()
#plt.show()

fname = 'client_network_throughput.png'
fig.savefig(fname)
fname = 'client_network_throughput.pdf'
fig.savefig(fname)


#######################
## Application throughput 
#######################

img_width = 10
img_height = 1*img_width
fig, axarr = plt.subplots(2, sharex=True)

tags = [ "sender", "receiver" ]
p=0
for suffix in tags:

	filename="iperf3_%s.log" % (suffix)
	try:
		with open(filename, 'r') as fh:
		  data = fh.read()
	except Exception as e:
		print(e)

	try:
		iperf = json.loads(data)
	except Exception as e:
		print(e)

	intervals=iperf.get('intervals')
	throughput=np.zeros([len(intervals),2])
	c=0
	for i in intervals:
		throughput[c]=[i['sum']['start'], i['sum']['bits_per_second']]
		c+=1

	axarr[p].plot(throughput[:,0],throughput[:,1]/1e6,'.',label=suffix)
	appl_mean=np.mean(throughput[:,1])/1e6
	axarr[p].plot([throughput[0,0], np.max(throughput[:,0])],[appl_mean, appl_mean],'-', label="average")
        axarr[p].set_ylabel('appl. throughput\n[MBit/s]')
	axarr[p].legend()
	p+=1

axarr[p-1].set_xlabel('time [s]')
axarr[p-1].set_xlim(0,45)

fig.set_size_inches(img_width, img_height)
fig.tight_layout()
#plt.show()

fname = 'appl_throughput.png'
fig.savefig(fname)

fname = 'appl_throughput.pdf'
fig.savefig(fname)
