#!/bin/bash
declare -a CONGESTIION_CONTROLS=("reno" "cubic" "bbr")
#declare -a CONGESTIION_CONTROLS=("reno")
declare -a SCHEDULER=("default")
#declare -a IPERF_BUFFER_LENGTHS=("1K" "128K")
declare -a IPERF_BUFFER_LENGTHS=("1K")
declare -a WMEM_INCS=("8" "16" "32" "64" "128" "256" "512")
#declare -a WMEM_INCS=("8" "512")
declare -a RMEM_INCS=("8" "16" "32" "64" "128")
#declare -a RMEM_INCS=("8" "256")
declare -a MPTCP_ENABLE=("1")
RUNS=1
