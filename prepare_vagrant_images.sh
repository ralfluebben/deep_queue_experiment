declare -a arr=("enb0" "enb1" "gateway")
for s in "${arr[@]}";
do
	echo $s
	vagrant ssh ${s} -c 'export DEBIAN_FRONTEND=noninteractive; sudo apt-get update; sudo -E apt-get -y -o "Dpkg::Options::=--force-confdef" -o "Dpkg::Options::=--force-confold" upgrade'
	vagrant ssh ${s} -c "sudo apt -y install tcpdump iperf3 trace-cmd"
	vagrant ssh ${s} -c "sudo apt -y install make gcc; wget https://mirrors.edge.kernel.org/pub/software/network/ethtool/ethtool-4.19.tar.gz; tar xvzf ethtool-4.19.tar.gz; cd ethtool-4.19/; ./configure; make; sudo make install; cd ..; rm -rf ethtool-4.19*"
done

declare -a arr=("client" "server")
for s in "${arr[@]}"
do
	echo $s;

	vagrant ssh ${s} -c "sudo apt -y install make gcc; wget https://mirrors.edge.kernel.org/pub/software/network/ethtool/ethtool-4.19.tar.gz; tar xvzf ethtool-4.19.tar.gz; cd ethtool-4.19/; ./configure; make; sudo make install; cd ..; rm -rf ethtool-4.19*"
	vagrant ssh ${s} -c "sudo apt update; sudo apt-get -y upgrade; sudo apt -y install dirmngr apt-transport-https tcpdump trace-cmd iperf3"

	vagrant ssh ${s} -c "sudo apt-key adv --keyserver hkp://keys.gnupg.net --recv-keys 379CE192D401AB61"
	vagrant ssh ${s} -c "sudo apt-key adv --keyserver hkp://keys.gnupg.net --recv-keys 379CE192D401AB61"
	vagrant ssh ${s} -c "sudo sh -c \"echo 'deb https://dl.bintray.com/multipath-tcp/mptcp_deb stable main' > /etc/apt/sources.list.d/mptcp.list\""
	vagrant ssh ${s} -c 'sudo sh -c "printf \"Package: *\nPin: origin dl.bintray.com\nPin-Priority: 1000\" > /etc/apt/preferences.d/mptcp"; cat /etc/apt/preferences.d/mptcp'
	vagrant ssh ${s} -c 'export DEBIAN_FRONTEND=noninteractive; sudo apt-get update; sudo apt-get -y --allow-downgrades --allow-unauthenticated install linux-mptcp; sudo -E apt-get -y --allow-downgrades --allow-unauthenticated -o "Dpkg::Options::=--force-confdef" -o "Dpkg::Options::=--force-confold" upgrade'
	vagrant ssh ${s} -c "sudo sed -i 's/GRUB_DEFAULT=0/GRUB_DEFAULT=saved/g' /etc/default/grub; sudo sed -i '7iGRUB_SAVEDEFAULT=true' /etc/default/grub; sudo sed -i '8iGRUB_DISABLE_SUBMENU=y' /etc/default/grub"
	bname=`vagrant ssh ${s} -c  "grep menuentry /boot/grub/grub.cfg" | awk -F"'" '{print $4}' | grep "advanced" | grep mptcp`
	vagrant ssh ${s} -c "sudo grub-set-default $bname; sudo update-grub;"
	vagrant ssh ${s} -c "sudo grub-mkconfig; sudo apt-get -y clean; sudo apt-get -y autoremove; cat /dev/null > ~/.bash_history; history -c"

done

declare -a arr=("enb0" "enb1" "client" "server" "gateway")
for s in "${arr[@]}"
do
	echo $s
	IP=`vagrant ssh ${s} -c "ip addr show eth0 | grep -Po 'inet \K[\d.]+'" | tr -d '[:space:]'`
	sshpass -p vagrant ssh-copy-id -o "UserKnownHostsFile=/dev/null" -o StrictHostKeyChecking=no vagrant@${IP}
done

