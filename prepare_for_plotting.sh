# IPerf creates a control and data connection for each subflows, filter the TCP SYN packets to get the IPs
ips=`tshark -t e -r server.pcap  -n -Y 'tcp.flags == 0x002 and tcp.dstport==5201'| sed 's/^[[:space:]]\{1,\}//g' | sed 's/  */ /g' | cut -d ' ' -f 5 | sort | uniq`

ip_1=`echo $ips | cut -f 1 -d ' '`
ip_2=`echo $ips | cut -f 2 -d ' '`

# We are only interested in the data connection, we have to SYN packets for each IP, the second one is the data connection.
#port_1=`tshark -t e -r server.pcap  -n -Y "tcp.flags == 0x002 and tcp.dstport==5201 and ip.dst==$ip_1"| sed 's/^[[:space:]]\{1,\}//g' | sed 's/  */ /g' | cut -d ' ' -f 8 | tail -n 1` 
#port_2=`tshark -t e -r server.pcap  -n -Y "tcp.flags == 0x002 and tcp.dstport==5201 and ip.dst==$ip_2"| sed 's/^[[:space:]]\{1,\}//g' | sed 's/  */ /g' | cut -d ' ' -f 8 | tail -n 1`

exec 3< <(tshark -t e -r server.pcap  -n -Y "tcp.flags == 0x002 and tcp.dstport==5201 and ip.dst==$ip_1"| sed 's/^[[:space:]]\{1,\}//g' | sed 's/  */ /g' | cut -d ' ' -f 8 | tail -n 1)
exec 4< <(tshark -t e -r server.pcap  -n -Y "tcp.flags == 0x002 and tcp.dstport==5201 and ip.dst==$ip_2"| sed 's/^[[:space:]]\{1,\}//g' | sed 's/  */ /g' | cut -d ' ' -f 8 | tail -n 1)

port_1=$(cat <&3)
port_2=$(cat <&4)

echo $port_1
echo $port_2

awk '/tcp_probe:/{split($0,a,"tcp_probe:"); n=split(a[1],b," "); gsub(":","",b[n]); gsub(/^[ \t]+/,"",b[n]); gsub("="," ",a[2]); split(a[2],c," "); printf "%s", b[n]; for (i in c) { printf " %s",c[i]}; print"" }' server_trace.txt > ttprobe.log.tmp2

grep $ip_1 ttprobe.log.tmp2 | grep $port_1 > ttprobe_1.csv &
grep $ip_2 ttprobe.log.tmp2 | grep $port_2 > ttprobe_2.csv &
wait
rm ttprobe.log.tmp2
tshark -t e -r server.pcap  -n -Y "ip.dst==$ip_1 and tcp.dstport==5201 and tcp.srcport==$port_1" | sed 's/^[[:space:]]\{1,\}//g' | sed 's/  */ /g' | cut -d ' ' -f 2,7 > throughput_1.csv &
tshark -t e -r server.pcap  -n -Y "ip.dst==$ip_2 and tcp.dstport==5201 and tcp.srcport==$port_2" | sed 's/^[[:space:]]\{1,\}//g' | sed 's/  */ /g' | cut -d ' ' -f 2,7 > throughput_2.csv &
tshark -t e -r server.pcap  -n -Y "tcp.dstport==5201 and (tcp.srcport==$port_1 or tcp.srcport==$port_2)" | sed 's/^[[:space:]]\{1,\}//g' | sed 's/  */ /g' | cut -d ' ' -f 2,7 > throughput.csv &

wait

tshark -t e -r client.pcap  -n -Y "ip.dst==$ip_1 and tcp.dstport==5201 and tcp.srcport==$port_1" | sed 's/^[[:space:]]\{1,\}//g' | sed 's/  */ /g' | cut -d ' ' -f 2,7 > client_throughput_1.csv &
tshark -t e -r client.pcap  -n -Y "ip.dst==$ip_2 and tcp.dstport==5201 and tcp.srcport==$port_2" | sed 's/^[[:space:]]\{1,\}//g' | sed 's/  */ /g' | cut -d ' ' -f 2,7 > client_throughput_2.csv &
tshark -t e -r client.pcap  -n -Y "tcp.dstport==5201 and (tcp.srcport==$port_1 or tcp.srcport==$port_2)" | sed 's/^[[:space:]]\{1,\}//g' | sed 's/  */ /g' | cut -d ' ' -f 2,7 > client_throughput.csv &

wait

